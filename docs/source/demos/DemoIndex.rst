=====================
Demo Gallery
=====================


Gaussian Toy Data Demos
=======================

.. raw:: html

	<div class="row">
	<div class="media">
	<div class="pull-left">
		<a href="GaussianToyData-FiniteMixtureModel-EM-SingleRunDemo.html">
			<img class="media-object" 
			src="../_static/GaussianToyData_FiniteMixtureModel_EM_SingleRunDemo_100x100.png"
			alt="...">
		</a>
	</div>
	<div class="media-body">
		<a href="GaussianToyData-FiniteMixtureModel-EM-SingleRunDemo.html">
		<h3 class="media-heading">
		Simple Gaussian mixture model on toy data
		</h3>
		</a>
		<p>
		Easy "hello-world" example for beginners to bnpy. Learn how to train a model and plot the results.
		</p>
	</div>
	</div>
	</div>


	<div class="row">
	<div class="media">
	<div class="pull-left">
		<a href="GaussianToyData-FiniteMixtureModel-EM-CompareInitialization.html">
			<img class="media-object" 
			src="../_static/GaussianToyData_FiniteMixtureModel_EM_CompareInitialization_100x100.png"
			alt="...">
		</a>
	</div>
	<div class="media-body">
		<a href="GaussianToyData-FiniteMixtureModel-EM-CompareInitialization.html">
		<h3 class="media-heading">
		Comparison of initialization methods for Gaussian mixtures
		</h3>
		</a>
		<p>
		Learn how to specify the initialization procedure and why all variational methods (from EM to memoized) are sensitive to this choice.
		</p>
	</div>
	</div>
	</div>



	<div class="row">
	<div class="media">
	<div class="pull-left">
		<a href="GaussianToyData-DPMixtureModel-MemoizedWithBirthsAndMerges.html">
			<img class="media-object" 
			src="../_static/GaussianToyData_DPMixtureModel_MemoizedWithBirthsAndMerges_100x100.png"
			alt="...">
		</a>
	</div>
	<div class="media-body">
		<a href="GaussianToyData-DPMixtureModel-MemoizedWithBirthsAndMerges.html">
		<h3 class="media-heading">
		Birth and merge inference for DP mixtures of Gaussians
		</h3>
		</a>
		<p>
		Experiment showing how bnpy's state-of-the-art birth and merge moves can find the ideal set of clusters, no matter how many we have initially.
		</p>
	</div>
	</div>
	</div>






Bag-of-words Toy Data Demos
===========================

.. image:: DemoIndex_files/DemoIndex_10_0.png



`Birth and merge inference for DP mixtures of Multinomials <./BarsToyData-DPMixtureModel-MemoizedWithBirthsAndMerges.html>`__
------------------------------------------------------------------------------------------------------------------------------

Experiment showing how births and merges add and remove clusters to find
the ideal set of bars.

.. image:: DemoIndex_files/DemoIndex_12_0.png



`Latent Dirichlet Allocation topic modeling with variational inference <./BarsToyData-FiniteTopicModel-Variational.html>`__
----------------------------------------------------------------------------------------------------------------------------

Experiment shows the basics for training a topic model, comparing
different number of topics.

.. image:: DemoIndex_files/DemoIndex_14_0.png



`Merge and delete moves with HDP topic models <./BarsToyData-HDPTopicModel-VariationalWithMergeDelete.html>`__
---------------------------------------------------------------------------------------------------------------

Use merge and delete moves for topic models to identify the 10 true bars
topics from initializations with many more.


.. image:: DemoIndex_files/DemoIndex_16_0.png
